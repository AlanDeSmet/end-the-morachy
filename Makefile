CODE=main.yaml
JSONCODE=$(CODE:.yaml=.json)
OUT=End_the_Monarchy.vtt
IN=/home/chaos/Downloads/$(OUT)
INNERFILE=$(shell unzip -l $(IN) | grep json | head -n1 | awk '{print $$4}' ) 

$(OUT): $(JSONCODE)
	jq . $^ > /dev/null
	rm $@
	zip $@ $^

.INTERMEDIATE: $(JSONCODE)

%.json: %.yaml
	./vtttool $^ > $@
	#yq eval -o=json $^ > $@

#%.yaml: %.json
#	yq eval -P $^ > $@


update: unpack
unpack: $(IN)
	git diff --exit-code $(CODE) > /dev/null
	mv $(IN) $(OUT)
	unzip -q $(OUT) $(INNERFILE)
	mv $(INNERFILE) $(JSONCODE)
	yq eval -P $(JSONCODE) > $(CODE)
	rm $(JSONCODE)

